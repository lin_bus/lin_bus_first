# snoop.ino
# rick - snoop on Lin-Bus packets
#include <LINBus_stack.h>


// Variables
const byte ident = 0; // Identification Byte
byte data_size=8; // length of byte array
byte data[8]; // byte array for received data

// LIN Objects
//lin_stack LIN1(1,ident); // Sniffer - reads Traffic on LIN Bus
//lin_stack LIN2(2); // Master - write data to LIN Bus

LINBus_stack stack=LINBus_stack();
LINBus_stack master=LINBus_stack();



void setup() {
  Serial.begin(9600);
  
  stack.setupSerial();
 
  //Serial.begin(9600); // Configure Serial for Serial Monitor
  //LIN1.setSerial(); // Configure Serial for receiving
}

void loop() {
  // Checking LIN Bus periodicly
  byte a = stack.readStream(data, data_size);
  
  if(a==1){ // If there was an event on LIN Bus, Traffic was detected. Print data to serial monitor
    master.writeStream(data, data_size); // write data to LIN Bus
    // Print received data, needed for monitoring, non-mandatory



    
    Serial.println("Traffic detected!");
    Serial.print("Synch Byte: ");
    Serial.println(data[0]);
    Serial.print("Ident Byte: ");
    Serial.println(data[1]);
    Serial.print("Data Byte1: ");
    Serial.println(data[2]);
    Serial.print("Data Byte2: ");
    Serial.println(data[3]);
    Serial.print("Data Byte3: ");
    Serial.println(data[4]);
    Serial.print("Data Byte4: ");
    Serial.println(data[5]);
    Serial.print("Data Byte5: ");
    Serial.println(data[6]);
    Serial.print("Check Byte: ");
    Serial.println(data[7]);
    Serial.print("\n");

    
  } 
}
