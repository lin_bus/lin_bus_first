# lin_bus_first

Refs:<br>
[https://github.com/gicking/LIN_master_Arduino](https://github.com/gicking/LIN_master_Arduino)<br>
[TJA1020 - datasheet](https://www.nxp.com/docs/en/data-sheet/TJA1020.pdf)<br>
[https://forum.arduino.cc/t/lin-bus-interfacing-tja1020/929595](https://forum.arduino.cc/t/lin-bus-interfacing-tja1020/929595)<br>
[https://github.com/macchina/LIN](https://github.com/macchina/LIN)<br>
[https://www.arduino.cc/reference/en/language/functions/communication/serial/](https://www.arduino.cc/reference/en/language/functions/communication/serial/)<br>
[https://github.com/macchina/LIN/blob/master/examples/Slave/Slave/Slave.ino](https://github.com/macchina/LIN/blob/master/examples/Slave/Slave/Slave.ino)<br>



![](t151.jpeg)<br>

# IMPORTANT NOTE
The trans and rec on the TTL serial side appear to be the wrong way round<br>


### Connections for connecting 2 T151 Boards
On the Lin side we need to connect + to 12V - to 0V and the Lin line<br>
On the TTL side we need to connect ground -> Arduino Ground, tx -> Arduino rx, rx->Arduino tx.<br>






### Arduino LinBus Library
[https://reference.arduino.cc/reference/en/libraries/linbus_stack/](https://reference.arduino.cc/reference/en/libraries/linbus_stack/)<br>
This is available in the library manager search for linbus-stack<br>


### Arduino Uno
Serial Ports<br>
0 - RX<br>
1 - TX<br>

### Software Serial
The circuit:<br>
 * RX is digital pin 10 (connect to TX of other device)<br>
 * TX is digital pin 11 (connect to RX of other device)<br>
